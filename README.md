## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Mon Feb 01 2021 14:39:03 GMT+0300 (GMT+03:00)|
|**App Generator**<br>@sap/generator-fiori-freestyle|
|**App Generator Version**<br>0.9.19|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>1worklist|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>http://10.41.11.62:8000/sap/opu/odata/sap/ZQM_FIORI_SRV
|**Module Name**<br>zqm_ui_qua_ent|
|**Application Title**<br>Onko|
|**Namespace**<br>com.arete|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Telemetry**<br>True|

## zqm_ui_qua_ent

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```


#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


