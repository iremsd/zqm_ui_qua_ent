sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, JSONModel, formatter, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("com.arete.zqmuiquaent.controller.Worklist", {

		formatter: formatter,


		onInit: function () {
			var oViewModel,

				oViewModel = new JSONModel({
					inputParti: false,
					inputUretimYeri: false,
					birim: false,
					tableData: [],
					BegdaCheck: false,
					EnddaCheck: false,
					searchButton:false

				});
			this.setModel(oViewModel, "worklistView");

		},

		onChangeMalzeme: function (oEvent) {
				var that=this;
				var oValue = oEvent.getParameter("value");
				that.getView().getModel("worklistView").setProperty("/searchButton", false);
				that.clearTable();
				if (oValue !== "") {
				var oDataModelMalzeme = this.getModel(),
				    secMalKontrol2 = this.byId("malzemeId").getSelectedKey(),
				    kontrolFilter2=[];
				    kontrolFilter2.push(new Filter("Malzeme", FilterOperator.EQ, secMalKontrol2));
				oDataModelMalzeme.read("/PartiStokSet", {
					filters: kontrolFilter2,
					
					success: function (data) {
						if(data.results.length){
							that.getView().getModel("worklistView").setProperty("/inputParti", true);
							that.getView().getModel("worklistView").setProperty("/inputUretimYeri", false);
							that.getView().getModel("worklistView").setProperty("/birim", false);
							that.getView().byId("partiId").setSelectedKey("");
							that.getView().byId("uretimId").setSelectedKey("");
							that.getView().byId("birimId").setSelectedKey("");
							
							
						}else{
							that.getView().getModel("worklistView").setProperty("/inputParti", false);
							that.getView().getModel("worklistView").setProperty("/inputUretimYeri", false);
							that.getView().getModel("worklistView").setProperty("/birim", false);
							that.getView().byId("partiId").setSelectedKey("");
							that.getView().byId("uretimId").setSelectedKey("");
							that.getView().byId("birimId").setSelectedKey("");

							
						}
						
	
					},
					error: function () {
	
					},
				});

			} 
		},
		onSelectChangeParti: function (oEvent) {
			var that=this;
			var oValue2 = oEvent.getParameter("selectedKey");
			that.getView().getModel("worklistView").setProperty("/searchButton", false);
			that.clearTable();
			if (oValue2 !== "") {

				this.getView().getModel("worklistView").setProperty("/inputUretimYeri", true);
				this.getView().getModel("worklistView").setProperty("/birim", false);
				that.getView().byId("uretimId").setSelectedKey("");
				that.getView().byId("birimId").setSelectedKey("");


			} else {
				this.getView().getModel("worklistView").setProperty("/inputUretimYeri", false);
				this.getView().getModel("worklistView").setProperty("/birim", false);
				that.getView().byId("uretimId").setSelectedKey("");
				that.getView().byId("birimId").setSelectedKey("");
			}
		},
		onSelectChangeUrtm: function (oEvent) {
			var that=this;
			var oValue3 = oEvent.getParameter("selectedItem").getKey();
			that.getView().getModel("worklistView").setProperty("/searchButton", false);
			that.clearTable();
			if (oValue3 !== "") {
			var oDataModel = this.getModel(),
			    secMalKontrol = this.byId("malzemeId").getSelectedKey(),
			    secUrtmYKontrol = this.byId("uretimId").getSelectedKey(),
			    secPrtiKontrol = this.byId("partiId").getSelectedKey(),
			kontrolFilter=[];
			kontrolFilter.push(new Filter("Malzeme", FilterOperator.EQ, secMalKontrol));
			kontrolFilter.push(new Filter("Parti", FilterOperator.EQ, secPrtiKontrol));
			kontrolFilter.push(new Filter("UretimYeri", FilterOperator.EQ, secUrtmYKontrol));
			oDataModel.read("/PartiStokSet", {
				filters: kontrolFilter,
				
				success: function (data) {
					if(data.results.length){
					that.getView().getModel("worklistView").setProperty("/birim", true);
					that.getView().byId("birimId").setSelectedKey("");
					}else{
						that.getView().getModel("worklistView").setProperty("/birim", false);
						that.getView().byId("birimId").setSelectedKey("");	
					}
					

				},
				error: function () {

				},
			});

			}
		},
		onSelectChangeBrm:function(oEvent)
		{
			var that=this;
			var oValue2 = oEvent.getParameter("selectedItem").getKey();
			that.clearTable();
			if (oValue2) {

				
				this.getView().getModel("worklistView").setProperty("/searchButton", true);
				


			} else {
				this.getView().getModel("worklistView").setProperty("/searchButton", false);	
			}
		},
		onSearch: function (oEvent) {
			var that = this;
			var oViewModel = this.getModel("worklistView"),
			 oDataModel = this.getModel(),
			 secMal = this.byId("malzemeId").getValue(),
			 secPrti = this.byId("partiId").getSelectedKey(),
			 secUrtmY = this.byId("uretimId").getSelectedKey(),
			 secBrm = this.byId("birimId").getSelectedKey();

			this.secMal = this.byId("malzemeId").getValue();
			this.secPrti = this.byId("partiId").getSelectedKey();
			this.secUrtmY = this.byId("uretimId").getSelectedKey();
			this.secBrm = this.byId("birimId").getSelectedKey();


			var aFilter = [];

			aFilter.push(new Filter("Malzeme", sap.ui.model.FilterOperator.EQ, secMal));
			aFilter.push(new Filter("UretimYeri", sap.ui.model.FilterOperator.EQ, secUrtmY));
			aFilter.push(new Filter("Parti", sap.ui.model.FilterOperator.EQ, secPrti));
			aFilter.push(new Filter("Birim", sap.ui.model.FilterOperator.EQ, secBrm));

			oDataModel.read("/BirimAdimSet", {
				filters: aFilter,
				method: "GET",
				success: function (data) {
					if (data.results.length) {
						/* data.results[0].Endda=new Date(); */
						
						
 						var tempArr = data.results;
						for (var i = 0; i < tempArr.length; i++) {
							tempArr[i].Malzeme = that.secMal;
							tempArr[i].UretimYeri = that.secUrtmY;
							tempArr[i].Parti = that.secPrti;
							tempArr[i].Birim = that.secBrm;
							tempArr[i].BegdaCheck = that.checkBegdacontrol(tempArr, i, tempArr[i].Begda); //aArr, iIndex, dBegda aşağıdaki fonksiyonuma arrayi bulunduğum indexi ve begdayı gönderdim
							tempArr[i].EnddaCheck = that.checkEnddacontrol(tempArr, i, tempArr[i].Endda);
						}
						that.getModel("worklistView").setProperty("/tableData", tempArr);
					}


				},
				error: function () {

				},
			});

		},
		baslangicSelect: function (oEvent) {

			var that = this;
			var oViewModel = this.getModel("worklistView"),
			 oSource = oEvent.getSource(),
			 path = oSource.getBindingContext("worklistView").getPath(),
			 iIndex = parseInt(path.split("/")[2], 10),
			 arrData = oViewModel.getData().tableData;
			this.sPath = oSource.getBindingContext("worklistView").getPath();
			var oBaslangicAn = new Date();
			var oNewEntry = {};
			oNewEntry.Kullanici = "DANISMANAR";
			oNewEntry.Malzeme = arrData[iIndex].Malzeme;
			oNewEntry.Parti = arrData[iIndex].Parti;
			oNewEntry.UretimYeri = arrData[iIndex].UretimYeri;
			oNewEntry.Birim = arrData[iIndex].Birim;
			oNewEntry.Adim = arrData[iIndex].Adim;
			oNewEntry.Tanim = arrData[iIndex].Tanim;
			oNewEntry.Begda = oBaslangicAn;
			oNewEntry.Endda = arrData[iIndex].Endda;
			var arrtableData= that.oView.getModel("worklistView").getData().tableData;

			var iflag=true;
			for(var i=0;i<iIndex;i++){
			if(arrtableData[i].Endda===null || arrtableData[i].Begda===null){
				sap.m.MessageToast.show(arrtableData[i].Adim+" adımındaki bitiş tarihi boş bırakılamaz");
				iflag=false;
				oEvent.getSource().setSelected(false);
				break;
			}else{
				iflag=true;	
			}
			}
			if(iflag){
			var oDataModel = this.getModel();
			oDataModel.create("/BirimAdimSet", oNewEntry, {
				success: function (oResp) {
					that.getModel("worklistView").setProperty(that.sPath, oResp);

					

				},
				error: function (oError) {
					alert("error");
				}
			});
		}
		},
		bitisSelect: function (oEvent) {
			var that = this;
			var oViewModel = this.getModel("worklistView"),
			 oSource = oEvent.getSource(),
			 path = oSource.getBindingContext("worklistView").getPath(),
			 iIndex = parseInt(path.split("/")[2], 10);
			that.iIndex = iIndex;
			var arrData = oViewModel.getData().tableData,
 			uplog= oSource.getBindingContext("worklistView").getObject("Logid"),
 			 upid = oSource.getBindingContext("worklistView").getObject("Id"),
			 upaltbid = oSource.getBindingContext("worklistView").getObject("Altbirimid");
			this.sPath = oSource.getBindingContext("worklistView").getPath("Endda");
			var oBitisAn = new Date();
			var oSecondEntry = {};
			oSecondEntry.Kullanici = "DANISMANAR";
 			oSecondEntry.Logid = uplog;
 			oSecondEntry.Malzeme = arrData[iIndex].Malzeme;
			oSecondEntry.Parti = arrData[iIndex].Parti;
			oSecondEntry.UretimYeri = arrData[iIndex].UretimYeri;
			oSecondEntry.Birim = arrData[iIndex].Birim;
			oSecondEntry.Adim = arrData[iIndex].Adim;
			oSecondEntry.Tanim = arrData[iIndex].Tanim;
			oSecondEntry.Begda = arrData[iIndex].Begda;
			oSecondEntry.Endda = oBitisAn;
			var arrtableData= that.oView.getModel("worklistView").getData().tableData;

			var iflag=true;
			for(var i=0;i<iIndex;i++){
			if(arrtableData[i].Endda===null){
				sap.m.MessageToast.show(arrtableData[i].Adim+" adımındaki bitiş tarihi boş bırakılamaz");
				iflag=false;
				oEvent.getSource().setSelected(false);
				break;
			}else{
				iflag=true;	
			}
			}
			
			if(iflag){
			var oDataModel = this.getModel();
			oDataModel.update("/BirimAdimSet(Id='" + upid + "',Altbirimid='" + upaltbid + "')", oSecondEntry, {
				success: function (oResp) {
					that.getModel("worklistView").setProperty(that.sPath, oBitisAn);
					var arrData2 = that.oView.getModel("worklistView").getData().tableData; 

					if (parseInt(that.iIndex) + 1 < arrData2.length) {
						var nextIndex = parseInt(that.iIndex) + 1;
						that.oView.getModel("worklistView").getData().tableData[nextIndex].BegdaCheck = true;
						that.oView.getModel("worklistView").refresh();

					}

				},
				error: function (oError) {
					alert("error");
				}
			});
		}
		},
		checkBegdacontrol: function (aArr, iIndex, dBegda) {
			

			if (iIndex === 0) {
				if (dBegda === null) {
					return true;
					
				} else {
					return false;
					
				}
			}
			else {

				if (aArr[iIndex - 1] !== undefined) {

					if(aArr[iIndex-1].Begda === null || aArr[iIndex-1].Begda !== null && aArr[iIndex-1].Endda === null)
					{
						return false;
					}
					else{
						return true;
					}




/* 
					 if (aArr[iIndex - 1].Begda === null) {
						return false;
					} else {
						
						return true;
					}  */
				}

			}

		},
		 checkEnddacontrol:function(eArr,eIndex,eEndda){


			if (eIndex === 0) {
				if (eArr[eIndex].Begda=== null && eArr[eIndex].Endda===null) {
					return false;
					
				} else if(eArr[eIndex].Begda!==null && eArr[eIndex].Endda===null) {
					return false;
					
				}
				else{
					return true;
				}
			}
			else {

				if (eArr[eIndex - 1] !== undefined) {
					if (eArr[eIndex - 1].Endda === null) {
						return false;
					} else {
						if(eArr[eIndex].Begda===null)
						{
							return false;
						}
						else{
							return true;
						}
						
						
					}
				}

			}
				
						
		},
		clearTable:function(){
			var tempArr=[];

			this.getModel("worklistView").setProperty("/tableData", tempArr);
		}



	});
});